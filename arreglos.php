<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Arreglos</title>
	<style>
		body{background-color: #B5CDE6; font-family: Arial; font-size: 4em; padding: 50px;}
	</style>
</head>
<body>
	<?php
		$frutas = $arrayName = array("platano","manzana","fresa","uvas");
		print_r($frutas);
		echo $frutas[1];
		echo "<br>";
		$numeroElementos=count($frutas);
		echo $numeroElementos . "<br>";

	 for ($i=0; $i < count($frutas); $i++) {
	 	echo $frutas[$i] . "<br>";
	 }

	 $edades = $arrayName = array('Eduardo' => 22,"Obed" => 21,"Bryan"=>24 );
	 print_r($edades);
	 echo "<br>";
	 echo $edades["Obed"];

	 foreach ($edades as $key => $value) {
	 	echo  $key . "Tiene la edad de". $value ."<br>";
	 }
	?>
</body>
</html>
